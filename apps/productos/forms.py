from django import forms
from multiupload.fields import MultiImageField

from apps.productos.models import Producto


class ProductoForm(forms.ModelForm):
    archivos = MultiImageField(min_num=1, max_num=10, max_file_size=1024 * 1024 * 5)

    class Meta:
        model = Producto
        fields = [
            'nombre',
            'descripcion',
            'modelo',
            'precio',
            'categoria',
            'archivos'
        ]


class ProductoEditForm(forms.ModelForm):
    class Meta:
        model = Producto
        fields = [
            'nombre',
            'descripcion',
            'modelo',
            'precio',
            'categoria'
        ]
