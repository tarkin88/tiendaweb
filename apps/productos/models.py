from django.core.exceptions import ValidationError
from django.db import models


class Categoria(models.Model):
    nombre = models.CharField(max_length=150)

    class Meta:
        db_table = "categorias"

    def __str__(self):
        return '{}'.format(self.nombre)


class Imagen(models.Model):
    def validate_file_extension(value):
        if not value.name.endswith('.jpg') or value.name.endswith('.jpeg'):
            raise ValidationError("Solo se permiten archivos JPG/JPEG")

    imagen = models.ImageField(upload_to="images/", validators=[validate_file_extension])

    class Meta:
        db_table = "imagenes"


class Producto(models.Model):
    nombre = models.CharField(max_length=150)
    descripcion = models.TextField()
    modelo = models.CharField(max_length=150)
    precio = models.DecimalField(decimal_places=2, max_digits=16)
    categoria = models.ManyToManyField(Categoria)
    imagenes = models.ManyToManyField(Imagen, blank=True, null=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    ultima_actualizacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{}".format(self.nombre)
