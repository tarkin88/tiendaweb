from django.contrib import messages
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic import CreateView
from django.views.generic import DeleteView
from django.views.generic import DetailView
from django.views.generic import TemplateView
from django.views.generic import UpdateView
from django_datatables_view.base_datatable_view import BaseDatatableView
from django.conf import settings

from apps.productos.forms import ProductoForm, ProductoEditForm
from apps.productos.models import Producto, Imagen


class ProductosList(BaseDatatableView):
    model = Producto
    columns = ['nombre', 'modelo', 'precio', 'id']
    order_columns = ['nombre', 'modelo', 'precio', '']
    max_display_length = 250

    def render_column(self, row, column):
        if column == 'precio':
            return '$ {}'.format(row.precio)
        else:
            return super(ProductosList, self).render_column(row, column)

    def filter_queryset(self, qs):
        search = self.request.GET.get(u'search[value]', None)
        if search:
            qs = qs.filter(Q(nombre__icontains=search) |
                           Q(modelo__icontains=search) |
                           Q(precio__icontains=search)
                           )
        return qs


class ProductosTemplateView(TemplateView):
    template_name = "productos/lista_productos.html"


class ProductoCreateView(CreateView):
    model = Producto
    template_name = "productos/producto_nuevo.html"
    form_class = ProductoForm
    success_url = reverse_lazy("lista_productos")

    def form_valid(self, form):
        super(ProductoCreateView, self).form_valid(form)
        for archivo in form.cleaned_data['archivos']:
            img = Imagen.objects.create(imagen=archivo)
            form.instance.imagenes.add(img)
        messages.success(self.request, "Producto agregado éxitosamente")
        return HttpResponseRedirect(self.get_success_url())


    def form_invalid(self, form):
        return super(ProductoCreateView, self).form_invalid(form)


class ProductoDetailView(DetailView):
    model = Producto
    template_name = "productos/producto_detalle.html"

    def get_context_data(self, **kwargs):
        context = super(ProductoDetailView, self).get_context_data(**kwargs)
        context['MEDIA_URL'] = settings.MEDIA_URL
        return context


class ProductoUpdateView(UpdateView):
    model = Producto
    template_name = "productos/producto_editar.html"
    form_class = ProductoEditForm
    success_url = reverse_lazy("lista_productos")

    def form_valid(self, form):
        messages.success(self.request, "Producto Editado éxitosamente")
        return super(ProductoUpdateView, self).form_valid(form)

    def form_invalid(self, form):
        return super(ProductoUpdateView, self).form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super(ProductoUpdateView, self).get_context_data(**kwargs)
        context['MEDIA_URL'] = settings.MEDIA_URL
        return context


class ProductoDeleteView(DeleteView):
    model = Producto
    success_url = reverse_lazy("lista_productos")
    template_name = "productos/producto_eliminar.html"
