# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-07 16:21
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('productos', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='producto',
            name='imagenes',
            field=models.ManyToManyField(null=True, to='productos.Imagen'),
        ),
    ]
