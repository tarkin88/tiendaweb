from django.contrib import admin

from apps.productos.models import Categoria, Producto

admin.site.register(Categoria)
admin.site.register(Producto)
