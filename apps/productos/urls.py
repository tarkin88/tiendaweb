from django.conf.urls import url

from apps.productos.views import ProductosTemplateView, ProductoCreateView, ProductosList, ProductoDetailView, \
    ProductoUpdateView, ProductoDeleteView

urlpatterns = [
    url(r'^productos_list_data/$', ProductosList.as_view(), name="productos_list_data"),
    url(r'^$', ProductosTemplateView.as_view(), name="lista_productos"),
    url(r'^nuevo/$', ProductoCreateView.as_view(), name="producto_nuevo"),
    url(r'^(?P<pk>\d+)/$', ProductoDetailView.as_view(), name="producto_detalle"),
    url(r'^(?P<pk>\d+)/editar$', ProductoUpdateView.as_view(), name="producto_editar"),
    url(r'^(?P<pk>\d+)/eliminar', ProductoDeleteView.as_view(), name="producto_eliminar"),
]
